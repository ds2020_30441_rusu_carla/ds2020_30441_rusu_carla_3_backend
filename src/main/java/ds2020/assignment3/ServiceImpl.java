package ds2020.assignment3;

import io.grpc.stub.StreamObserver;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceImpl extends ServiceGrpc.ServiceImplBase {

    @Override
    public void hello(
            HelloRequest request, StreamObserver<HelloResponse> responseObserver) {

        String greeting = new StringBuilder()
                .append("Hi, ")
                .append(request.getFirstName())
                .append(" ")
                .append(request.getLastName())
                .toString();

        HelloResponse response = HelloResponse.newBuilder()
                .setGreeting(greeting)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getMedicationPlan(
            MedicationPlanRequest request, StreamObserver<MedicationPlanResponse> responseObserver) {

        String url = "jdbc:postgresql://ec2-54-228-170-125.eu-west-1.compute.amazonaws.com:5432/d1glhli2hv3nki";
        String user = "hlfkhoexulejcr";
        String password = "e76429c313ef4a8a9b40156fdc6afd7ceb713a32d9de6c95ce8574a59a8b7f3c";

        String query = "SELECT medication.name, medication.dosage, medication_distribution.intake_interval " +
                "FROM medication " +
                "INNER JOIN medication_distribution " +
                "ON medication.id = medication_distribution.medication " +
                "INNER JOIN medication_plan " +
                "ON medication_distribution.medication_plan = medication_plan.id " +
                "WHERE medication_plan.patient = 1;";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            boolean isResult = preparedStatement.execute();
            do {
                try (ResultSet resultSet = preparedStatement.getResultSet()) {

                    while (resultSet.next()) {

                        MedicationPlanResponse response = MedicationPlanResponse.newBuilder()
                                .setName(resultSet.getString(1))
                                .setDosage(resultSet.getString(2))
                                .setIntakeInterval(resultSet.getString(3))
                                .build();

                        responseObserver.onNext(response);

//                        System.out.print(response.getName());
//                        System.out.print(": ");
//                        System.out.print(response.getDosage());
//                        System.out.print(": ");
//                        System.out.println(response.getIntakeInterval());
                    }

                    isResult = preparedStatement.getMoreResults();
                }
            } while (isResult);

        } catch (SQLException ex) {

            Logger logger = Logger.getLogger(ServiceImpl.class.getName());
            logger.log(Level.SEVERE, ex.getMessage(), ex);
        }

        responseObserver.onCompleted();
    }

    @Override
    public void take(TakeRequest request, StreamObserver<TakeResponse> responseObserver) {
        System.out.println("Took medicine: "+request.getName());
        TakeResponse response = TakeResponse.newBuilder()
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void notTake(NotTakeRequest request, StreamObserver<NotTakeResponse> responseObserver) {
        System.out.println("Did not take medicine: "+request.getName());
        NotTakeResponse response = NotTakeResponse.newBuilder()
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void time(TimeRequest request, StreamObserver<TimeResponse> responseObserver) {
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimeResponse response = TimeResponse.newBuilder()
                .setHours(Integer.toString(hour))
                .setMinutes(Integer.toString(minute))
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
